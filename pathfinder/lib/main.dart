import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:pathfinder/login/home.dart';
import 'package:pathfinder/login/login.dart';
import 'package:pathfinder/drawer/home.dart';
import 'package:pathfinder/welcome/welcome.dart';
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pathfinder',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      darkTheme: ThemeData.dark(),
      home: Welcome(),
    );
  }
}
