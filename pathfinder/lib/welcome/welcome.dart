
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:pathfinder/login/login.dart';



class Welcome extends StatefulWidget {

   Welcome({Key key}) : super(key: key);
  

  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  List<Slide> slides = [];
  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "Pathfinder",
        description: "We carefully verify all information before adding them into the app",
        pathImage: "assets/images/path.png",
        backgroundColor: Color(0xff203152),
      ),
    );
    slides.add(
      new Slide(
        title: "UTM",
        description: "Find deals and book great value fares to 60+ destinations worldwide. Malaysia Airlines is the national carrier of Malaysia",
        pathImage: "assets/images/utm.jpg",
        backgroundColor: Color(0xff203152),
      ),
    );
    slides.add(
      new Slide(
        title: "Johor",
        description:
            "We carefully verify all information before adding them into the app",
        pathImage: "assets/images/johor.jpg",
        backgroundColor: Color(0xff9932CC),
      ),
    );
  }

  void onDonePress() {

    Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Login()));
    // Do what you want
    print("End of slides");
  }
  Widget build(BuildContext context) {
    return IntroSlider(
      slides: this.slides,
      onDonePress: this.onDonePress,
      
    );
  }
}