class PlanetInfo {
  final int position;
  final String name;
  final String iconImage;
  final String description;
  final List<String> images;

  PlanetInfo(
    this.position, {
    this.name,
    this.iconImage,
    this.description,
    this.images,
  });
}

List<PlanetInfo> planets = [
  PlanetInfo(1,
      name: 'Legoland Malaysia',
      iconImage: 'assets/images/legoland.jpg',
      description:
          "",
      images: [

        
        
      ]),
  PlanetInfo(2,
      name: 'Johor Zoo',
      iconImage: 'assets/images/zoo.jpg',
      description:
          "",
      images: [
        
      ]),
  PlanetInfo(3,
      name: 'Danga',
      iconImage: 'assets/images/danga.jpg',
      description:
          "",
      images: [
        
      ]),
  
];