import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class Explore extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      
      title: "Explore Places",
      home: Scaffold(
        
        appBar:AppBar(
          
          
          title: Text('Explore Places'),
          backgroundColor: Color(0xFFFF1744),
          ),

          body: WebView(

            initialUrl: "https://www.google.com/travel/things-to-do?g2lb=2502548%2C2503771%2C2503781%2C2503895%2C4258168%2C4270442%2C4306835%2C4317915%2C4371335%2C4401769%2C4419364%2C4429192%2C4509341%2C4515404%2C4545890%2C4561952%2C4588450%2C4270859%2C4284970%2C4291517&hl=en-MY&gl=my&ssta=1&dest_mid=%2Fm%2F0207sh&dest_state_type=main&dest_src=ts&sa=X&ved=2ahUKEwjnv_y91oPyAhWf63MBHUUvBAIQuL0BMAl6BAgCECw#ttdm=1.479836_103.660513_11&ttdmf=%252Fg%252F11cs5_431n",
            javascriptMode: JavascriptMode.unrestricted,
          ),
          
          ),
      
    );
  }
}