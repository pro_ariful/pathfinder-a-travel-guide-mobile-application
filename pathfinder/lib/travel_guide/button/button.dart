import 'package:flutter/material.dart';
import 'package:pathfinder/travel_guide/guide/KLIA1_KLI2.dart';
import 'package:pathfinder/travel_guide/guide/KLIA_KLCITY.dart';
import 'package:pathfinder/travel_guide/guide/KLIA_TBS.dart';
import 'package:pathfinder/travel_guide/guide/TBS_JB.dart';
import 'package:pathfinder/travel_guide/guide/TBS_JBTUN.dart';
import 'package:pathfinder/travel_guide/guide/tun_larking.dart';




class TravelGuideButton extends StatefulWidget {  
  @override  
  _TravelGuideButtonState createState() => _TravelGuideButtonState();  
}  
  
class _TravelGuideButtonState extends State<TravelGuideButton> {  
  @override  
  Widget build(BuildContext context) {  
    return MaterialApp(  
      home: Scaffold(  
          appBar: AppBar(  
            title: Text('Travel Guide'),  
            backgroundColor: Color(0xFFFF1744),
          ),  
          body: Center(child: ListView(children: <Widget>[  
             
            ListTile(  
               
              title: OutlineButton(  
                child: Text("KLIA1 to KLIA2", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Travelguide1()));
                },  
              ),  
            ), 

            ListTile(  
               
              title: OutlineButton(  
                child: Text("KLIA to TBS", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Travelguide2()));
                },  
              ),  
            ), 

            ListTile(  
               
              title: OutlineButton(  
                child: Text("KLIA to KL City", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Travelguide3()));
                },  
              ),  
            ), 
            

            ListTile(  
               
              title: OutlineButton(  
                child: Text("TBS to JB Larkin", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Travelguide4()));
                },  
              ),  
            ), 
            
            ListTile(  
               
              title: OutlineButton(  
                child: Text("TBS to JB Tun Aminah", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Travelguide5()));
                },  
              ),  
            ), 

            ListTile(  
               
              title: OutlineButton(  
                child: Text("JB Larkin to JB Tun Aminah", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Travelguide6()));
                },  
              ),  
            ), 
             
          ]  
          ))  
      ),  
    );  
  }  
}  