import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class TravalGuide extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      
      title: "Pathfinder Travel Details Guide",
      home: Scaffold(
        
        appBar:AppBar(
          
          
          title: Text('Pathfinder Travel Details Guide'),
          backgroundColor: Color(0x000000),
          ),

          body: WebView(

            initialUrl: "https://wego.here.com/",
            javascriptMode: JavascriptMode.unrestricted,
          ),
          
          ),
      
    );
  }
}