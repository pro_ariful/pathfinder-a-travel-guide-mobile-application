import 'package:flutter/material.dart';
import 'package:pathfinder/contact_tracing/positive/positive.dart';
import 'package:pathfinder/contact_tracing/tracing/tracing.dart';


class RaiasedButton extends StatefulWidget {  
  @override  
  _RaiasedButtonState createState() => _RaiasedButtonState();  
}  
  
class _RaiasedButtonState extends State<RaiasedButton> {  
  @override  
  Widget build(BuildContext context) {  
    return MaterialApp(  
      home: Scaffold(  
          appBar: AppBar(  
            title: Text('Pathfinder Contact Tracing'),  
          ),  
          body: Center(child: Column(children: <Widget>[  
            Container(  
              margin: EdgeInsets.all(25),  
              child: OutlineButton(  
                child: Text("+ve COVID", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                   Navigator.push(
                    context, MaterialPageRoute(builder: (_) =>Positive()));
                },  
              ),  
            ),  
            Container(  
              margin: EdgeInsets.all(25),  
              child: OutlineButton(  
                child: Text("Trace Nearby People", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) =>Tracing()));
                },  
              ),  
            ), 
             
          ]  
          ))  
      ),  
    );  
  }  
}  