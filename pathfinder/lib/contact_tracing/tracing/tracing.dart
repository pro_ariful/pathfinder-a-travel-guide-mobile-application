import 'package:firebase_core/firebase_core.dart';
import 'package:pathfinder/contact_tracing/tracing/src/views/home_page.dart';
import 'package:pathfinder/contact_tracing/tracing/src/views/marker_page.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(Tracing());
}

class Tracing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: 'home',
      routes: {
        'home': (context) => HomePage(),
        'marker': (context) => MarkerPage()
      },
    );
  }
}