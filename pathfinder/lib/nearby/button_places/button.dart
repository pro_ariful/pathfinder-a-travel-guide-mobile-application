import 'package:flutter/material.dart';
import 'package:pathfinder/nearby/airport/airport.dart';
import 'package:pathfinder/nearby/atm/atm.dart';
import 'package:pathfinder/nearby/bank/bank.dart';
import 'package:pathfinder/nearby/bus_station/bus.dart';
import 'package:pathfinder/nearby/cafe/cafe.dart';
import 'package:pathfinder/nearby/clothing_store/cloathing.dart';
import 'package:pathfinder/nearby/embassy/embasy.dart';
import 'package:pathfinder/nearby/museam/museam.dart';
import 'package:pathfinder/nearby/pharmacy/pharmacy.dart';
import 'package:pathfinder/nearby/post_office/post_office.dart';
import 'package:pathfinder/nearby/primary_school/primary.dart';
import 'package:pathfinder/nearby/pumber/plumber.dart';
import 'package:pathfinder/nearby/school/school.dart';
import 'package:pathfinder/nearby/secondary_school/secondary_school.dart';
import 'package:pathfinder/nearby/shopping_mall/mall.dart';
import 'package:pathfinder/nearby/stadium/stadium.dart';
import 'package:pathfinder/nearby/taxi_stand/taxi.dart';
import 'package:pathfinder/nearby/train_station/train.dart';
import 'package:pathfinder/nearby/uni/uni.dart';
import 'package:pathfinder/nearby/university/university.dart';  
  
 
  
class NearbyPlacesButton extends StatefulWidget {  
  @override  
  _NearbyPlacesButtonState createState() => _NearbyPlacesButtonState();  
}  
  
class _NearbyPlacesButtonState extends State<NearbyPlacesButton> {  
  @override  
  Widget build(BuildContext context) {  
    return MaterialApp(  
      home: Scaffold(  
          appBar: AppBar(  
            title: Text('Nearby Other Places'),  
            backgroundColor: Color(0xFFFF1744),
          ),  
          body: Center(child: ListView(children: <Widget>[  
             
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Airport", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Airport()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Bank", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Bank()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Bus Stand", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => BusStand()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Train Station", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => TrainStand()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Taxi Stand", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => TaxiStand()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Pharmacy", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Pharmacy()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby ATM", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => ATM()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Primary School", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Primary()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby School", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => School()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Secondary School", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => SecendarySchool()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Shopping Mall", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Mall()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Cafe", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Cafe()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Clothing", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => ClothsStand()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Embassy", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Embassy()));
                },  
              ),  
            ), 
            
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Museum", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Museum()));
                },  
              ),  
            ), 
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Plumber", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Plumber()));
                },  
              ),  
            ), 
             ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Post Office", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => PostOffice()));
                },  
              ),  
            ),
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby Stadium", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Stadium()));
                },  
              ),  
            ),
            
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Nearby University", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Uni()));
                },  
              ),  
            ),
          ]  
          ))  
      ),  
    );  
  }  
}  