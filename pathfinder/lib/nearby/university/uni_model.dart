import 'package:google_maps_flutter/google_maps_flutter.dart';

class Coffee {
  String uniName;
  String address;
  String description;
  String thumbNail;
  LatLng locationCoords;

  Coffee(
      {this.uniName,
      this.address,
      this.description,
      this.thumbNail,
      this.locationCoords});
}

final List<Coffee> coffeeShops = [
  Coffee(
      uniName: 'Universiti Teknologi Malaysia',
      address: '81310 Skudai, Johor',
      description:
          'University of Technology Malaysia is a premier Malaysian public research-intensive university',
      locationCoords: LatLng(1.5594, 103.6386),
      thumbNail: 'https://media.glassdoor.com/l/a0/6e/5a/b7/main-gate-utm.jpg'
      ),

      Coffee(
      uniName: 'Raffles University',
      address: '80000 Johor Bahru, Johor',
      description:
          'Raffles University is located in Johor, Malaysia.',
      locationCoords: LatLng(1.4595, 103.7625),
      thumbNail: 'https://raffles-university.edu.my/wp-content/uploads/2020/09/RU-logo-scaled.jpg'
      ),

      Coffee(
      uniName: 'University of Southampton ',
      address: '79200 Nusajaya, Johor',
      description:
          'University of Southampton Malaysia is one of the best engineering universities',
      locationCoords: LatLng(1.4301, 103.6116),
      thumbNail: 'https://www.thesundaily.my/binrepository/768x511/0c40/768d432/none/11808/IITN/IMG_0958_v0_c1108634_14710_947_ARCH261905_MG1550497.jpg'
      ),

      Coffee(
      uniName: 'INTI Education Center',
      address: ' 81100 Johor Bahru, Johor',
      description:
          'INTI Malaysia is one of the best universities',
      locationCoords: LatLng(1.5615529780605277, 103.76890726771167),
      thumbNail: 'https://www.thesundaily.my/binrepository/768x511/0c40/768d432/none/11808/IITN/IMG_0958_v0_c1108634_14710_947_ARCH261905_MG1550497.jpg'
      ),

      Coffee(
      uniName: 'Sunway College',
      address: ' 81100 Johor Bahru, Johor',
      description:
          'INTI Malaysia is one of the best universities',
      locationCoords: LatLng(1.5630212476749181, 103.7921094089739),
      thumbNail: 'https://1.bp.blogspot.com/-iJeDfeuTrgk/XYC7h6nRWxI/AAAAAAAAPTk/kPME2bgeNJ4CcVqQIRPr2SM5ARWbZkDfgCLcBGAsYHQ/s1600/WhatsApp%2BImage%2B2019-09-17%2Bat%2B18.54.03.jpeg'
      ),

      Coffee(
      uniName: 'MMU Johor',
      address: '79200 Iskandar Puteri, Johor',
      description:
          'A Premier Digital Tech University and being a trendsetter of the private higher learning provider in Malaysia',
      locationCoords: LatLng(1.4313590270218586, 103.60595512695753),
      thumbNail: 'https://1.bp.blogspot.com/-iJeDfeuTrgk/XYC7h6nRWxI/AAAAAAAAPTk/kPME2bgeNJ4CcVqQIRPr2SM5ARWbZkDfgCLcBGAsYHQ/s1600/WhatsApp%2BImage%2B2019-09-17%2Bat%2B18.54.03.jpeg'
      ),

      Coffee(
      uniName: 'City University Johor',
      address: '80150 Johor Bahru, Johor',
      description:
          'City University Malaysia was founded by a group of scholars with extensive experience in local and international universities in April 1984.',
      locationCoords: LatLng(1.4980089002110666, 103.76557054638619),
      thumbNail: 'https://pbs.twimg.com/media/EViim8uUUAA55YS.jpg'
      ),

      Coffee(
      uniName: 'City University Malaysia',
      address: '80150 Johor Bahru, Johor',
      description:
          'City University Malaysia was founded by a group of scholars with extensive experience in local and international universities in April 1984.',
      locationCoords: LatLng(1.4998643549625783, 103.7674481013722),
      thumbNail: 'https://pbs.twimg.com/media/EViim8uUUAA55YS.jpg'
      ),

      Coffee(
      uniName: 'Crescendo International College',
      address: '81800 Ulu Tiram, Johor',
      description:
          'Crescendo International College is a college in Desa Cemerlang, Mukim Tebrau, Johor Bahru, Johor.',
      locationCoords: LatLng(1.5575601817408202, 103.81249805908732),
      thumbNail: 'https://edufair.fsi.com.my/img/sponsor/95/cover_1534991103.jpeg'
      ),

      Coffee(
      uniName: 'Asia Metropolitan University',
      address: '81750 Masai, Johor',
      description:
          "Gain strategic business & management skills to launch a career with greater performance at Malaysia's fastest growing ASIC accredited university",
      locationCoords: LatLng(1.5105297135095102, 103.8694860725046),
      thumbNail: 'https://university.tuitionjob.com/custom/picture/177/uni_58c3b8572890b.png'
      ),

      Coffee(
      uniName: 'MSU College, Johor',
      address: '81200 Johor Bahru, Johor',
      description:
          "MSU College Johor Bahru (JB) is an institution of higher education committed to meeting the educational and professional needs of its students.",
      locationCoords: LatLng(1.5209467119450217, 103.7506326858436),
      thumbNail: 'https://pbs.twimg.com/profile_images/1218798743065001985/Ehl5BsXw_400x400.jpg'
      ),

      Coffee(
      uniName: 'Monash University',
      address: '80000 Johor Bahru, Johor',
      description:
          "Embark on an adventure, immerse yourself in student life and local culture, and earn a prestigious internationally-recognised degree",
      locationCoords: LatLng(1.4591773974706626, 103.74925991041181),
      thumbNail: 'https://www.monash.edu.my/__data/assets/image/0010/1211959/sfb-main-lp-01-about-us-02-csjb.jpg'
      ),

      Coffee(
      uniName: 'Olympia College',
      address: '81100 Johor Bahru, Johor',
      description:
          'Olympia College (Johor Bahru). Higher institutes of learning should be made accessible to all.',
      locationCoords: LatLng(1.465373472415937, 103.75375372816639),
      thumbNail: 'https://university.tuitionjob.com/custom/picture/186/uni_58c3f6f5a6e50.jpg'
      ), 

      Coffee(
      uniName: 'Asia e University',
      address: '81300 Skudai, Johor',
      description:
          "AeU Part-Time MBA Programs in Johor Bahru. it's offered in weekend modes. AeU was established by Asians for Asia under the Asia Cooperation.",
      locationCoords: LatLng(1.5400059341117172, 103.63159817238656),
      thumbNail: 'https://s3.eu-north-1.amazonaws.com/images.free-apply.com/uni/gallery/lg/1045800003/ccadf29850e32b7e53758154fee662935a10ab45.jpg'
      ),  

      Coffee(
      uniName: 'UNITAR Int University',
      address: '80000 Johor Bahru',
      description:
          "Known as the fist virtual university in Southeast Asia, UNITAR is a visionary institution of choice to pursue a wide range of academic programmes.",
      locationCoords: LatLng(1.4566063944970176, 103.76373127684295),
      thumbNail: 'https://www.unitar.my/images/main/img-main-about.jpg'
      ),  
      Coffee(
      uniName: 'Southern University College',
      address: '81300 Skudai, Johor',
      description:
          "Southern University College, abbreviated as Southern UC, is a non-profit, private university college in Malaysia",
      locationCoords: LatLng(1.53398066001362, 103.6821441969843),
      thumbNail: 'https://www.easyuni.com/media/institution/photo/2019/07/08/14969782931755597496.JPG.600x400_q85.jpg'
      ), 
      Coffee(
      uniName: 'Reliance College',
      address: '80400 Johor Bahru, Johor',
      description:
          "Reliance College is a non-profit, private university college in Malaysia",
      locationCoords: LatLng(3.1252613438897705, 101.71106797468329),
      thumbNail: 'https://www.easyuni.my/media/institution/photo/2017/12/14/facilities_01.jpg.600x400_q85.jpg'
      ), 
      Coffee(
      uniName: 'University of Reading MY',
      address: '79200 Nusajaya, Johor',
      description:
          "Welcome to the University of Reading Malaysia. Find your undergraduate, postgraduate or research degree and study the subject you're passionate about.",
      locationCoords: LatLng(1.430521551005327, 103.61587849700545),
      thumbNail: 'https://www.asianstudycentre.com/ascnews/wp-content/uploads/2017/01/011.jpg'
      ), 

      Coffee(
      uniName: 'Kolej MDIS Malaysia',
      address: '79200 Nusajaya, Johor',
      description:
          "Kolej MDIS Malaysia is the first private tertiary institution of Singapore to make its foray into EduCity Iskandar Puteri in Johor.",
      locationCoords: LatLng(1.4327579484876467, 103.61557967463816),
      thumbNail: 'https://www.mdis.edu.my/wp-content/uploads/2020/02/admision-1.png'
      ),

      Coffee(
      uniName: 'Universiti Utara Malaysia',
      address: '81100 Johor Bahru, Johor',
      description:
          "Universiti Utara Malaysia, UUM. ... Universiti Utara Malaysia, the Eminent Management University.",
      locationCoords: LatLng(1.5265168361078956, 103.78849807252791),
      thumbNail: 'https://www.easyuni.my/media/institution/photo/2016/04/21/3-universiti-utara-malaysia.jpg.600x400_q85.jpg'
      ),
];