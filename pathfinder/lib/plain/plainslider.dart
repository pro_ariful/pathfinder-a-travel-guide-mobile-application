
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:pathfinder/plain/plainlist.dart';
import 'airasia.dart';


class PlainSlider extends StatefulWidget {

   PlainSlider({Key key}) : super(key: key);
  

  @override
  _PlainSliderState createState() => _PlainSliderState();
}

class _PlainSliderState extends State<PlainSlider> {
  List<Slide> slides = [];
  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "Air Asia",
        description: "Let your dreams come true when you book a cheap ticket to Malaysia with AirAsia! Dream big",
        pathImage: "assets/images/air.png",
        backgroundColor: Color(0xfff5a623),
      ),
    );
    slides.add(
      new Slide(
        title: "Malaysia Airlines",
        description: "Find deals and book great value fares to 60+ destinations worldwide. Malaysia Airlines is the national carrier of Malaysia",
        pathImage: "assets/images/myair.png",
        backgroundColor: Color(0xff203152),
      ),
    );
    slides.add(
      new Slide(
        title: "Malindo Airlines",
        description:
            "Malindo Air is a Malaysian premium airline with headquarters in Petaling Jaya, Selangor, Malaysia.",
        pathImage: "assets/images/malindo.png",
        backgroundColor: Color(0xff9932CC),
      ),
    );
  }

  void onDonePress() {

    Navigator.push(
                    context, MaterialPageRoute(builder: (_) => PlanListButton()));
    // Do what you want
    print("End of slides");
  }
  Widget build(BuildContext context) {
    return IntroSlider(
      slides: this.slides,
      onDonePress: this.onDonePress,
      
    );
  }
}