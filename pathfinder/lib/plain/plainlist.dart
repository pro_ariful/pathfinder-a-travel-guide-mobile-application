
import 'package:flutter/material.dart';
import 'package:pathfinder/plain/airasia.dart';
import 'package:pathfinder/plain/cheapflights.dart';
import 'package:pathfinder/plain/malindo.dart';
import 'package:pathfinder/plain/my_airlines.dart';




class PlanListButton extends StatefulWidget {  
  @override  
  _PlanListButtonState createState() => _PlanListButtonState();  
}  
  
class _PlanListButtonState extends State<PlanListButton> {  
  @override  
  Widget build(BuildContext context) {  
    return MaterialApp(  
      home: Scaffold(  
          appBar: AppBar(  
            title: Text('Buy Plain Tickets'),  
            backgroundColor: Color(0xFFFF1744),
          ),  
          body: Center(child: ListView(children: <Widget>[  
             
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Air Asia", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => AirAsia()));
                },  
              ),  
            ), 

            ListTile(  
               
              title: OutlineButton(  
                child: Text("Malindo Airlines", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Malindo()));
                },  
              ),  
            ),

            ListTile(  
               
              title: OutlineButton(  
                child: Text("Malaysian Airlines", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => MalaysiaAir()));
                },  
              ),  
            ),
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Cheap Flights", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => CheapFlights()));
                },  
              ),  
            ),
            
           
             
          ]  
          ))  
      ),  
    );  
  }  
}  