import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class CheapFlights extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      
      title: "Pathfinder for Cheap Flights",
      home: Scaffold(
        
        appBar:AppBar(
          
          
          title: Text('Buy Air Ticket Online'),
          backgroundColor: Color(0xFFFF1744),
          ),

          body: WebView(

            initialUrl: "https://www.cheapflights.com.my/",
            javascriptMode: JavascriptMode.unrestricted,
          ),
          
          ),
      
    );
  }
}