import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pathfinder/travel_blog/models/notes_operation.dart';
import 'package:pathfinder/travel_blog/screen/home_screen.dart';

void main() => runApp(TravelBlog());

class TravelBlog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<NotesOperation>(
      create: (context) => NotesOperation(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomeScreen(),
      ),
    );
  }
}