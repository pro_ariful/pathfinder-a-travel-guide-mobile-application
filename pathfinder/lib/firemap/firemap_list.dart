
import 'package:flutter/material.dart';
import 'package:pathfinder/firemap/fire_maps.dart';
import 'package:pathfinder/firemap/firemap.dart';



class HeatMapsButton extends StatefulWidget {  
  @override  
  _HeatMapsButtonState createState() => _HeatMapsButtonState();  
}  
  
class _HeatMapsButtonState extends State<HeatMapsButton> {  
  @override  
  Widget build(BuildContext context) {  
    return MaterialApp(  
      home: Scaffold(  
          appBar: AppBar(  
            title: Text('Find Heat Maps of COVID-19'),  
            backgroundColor: Color(0xFFFF1744),
          ),  
          body: Center(child: ListView(children: <Widget>[  
             
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Fire Maps", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => FiresMaps()));
                },  
              ),  
            ), 

            ListTile(  
               
              title: OutlineButton(  
                child: Text("Heat Maps", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => FireMap()));
                },  
              ),  
            ),

           
           
             
          ]  
          ))  
      ),  
    );  
  }  
}  