import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class FireMap extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      
      title: "Fire Map",
      home: Scaffold(
        
        appBar:AppBar(
          
          
          title: Text('Fire Map'),
          backgroundColor: Color(0xFFFF1744),
          ),

          body: WebView(

            initialUrl: "https://www.arcgis.com/apps/View/index.html?appid=b5310fd4a23d4fe288d8bf68edecac3d",
            javascriptMode: JavascriptMode.unrestricted,
          ),
          
          ),
      
    );
  }
}