import 'package:flutter/material.dart';
import 'package:pathfinder/contact_tracing/cvoidbutton.dart';
import 'package:pathfinder/covid_tracker/covid.dart';
import 'package:pathfinder/currency/currency.dart';
import 'package:pathfinder/explore_places/exporeplaces.dart';
import 'package:pathfinder/explore_places/home.dart';
import 'package:pathfinder/firemap/fire_maps.dart';
import 'package:pathfinder/firemap/firemap.dart';
import 'package:pathfinder/firemap/firemap_list.dart';
import 'package:pathfinder/login/login.dart';
import 'package:pathfinder/nearby/button_places/button.dart';
import 'package:pathfinder/nearby/hospital/hospital.dart';
import 'package:pathfinder/nearby/hotel/hotel.dart';
import 'package:pathfinder/nearby/mosque/mosque.dart';
import 'package:pathfinder/nearby/parking/parking.dart';
import 'package:pathfinder/nearby/police/police.dart';
import 'package:pathfinder/nearby/resturants/resturants.dart';
import 'package:pathfinder/nearby/tourist_attraction/tourist.dart';
import 'package:pathfinder/nearby/university/university.dart';
import 'package:pathfinder/bus/slider.dart';
import 'package:pathfinder/notes/addnote.dart';
import 'package:pathfinder/places/places.dart';
import 'package:pathfinder/plain/plainslider.dart';
import 'package:pathfinder/profile/profile.dart';
import 'package:pathfinder/push_notification/push.dart';
import 'package:pathfinder/search_places/nearby_places.dart';
import 'package:pathfinder/travel_blog/blog.dart';
import 'package:pathfinder/travel_guide/button/button.dart';
import 'package:pathfinder/travel_guide/webview_here_maps/travelGuide.dart';



class DrawerData extends StatelessWidget {
  const DrawerData();
  @override
  Widget build(BuildContext context) {
    final List<DrawerItem> drawer = [
      DrawerItem('Profile', Icons.account_circle),
      DrawerItem('Messages', Icons.message),
      DrawerItem('Activity', Icons.compare_arrows),
      DrawerItem('Bookmarks', Icons.bookmark),
      DrawerItem('Sign Out', Icons.exit_to_app),
    ];
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(24, 48, 24, 12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset('assets/images/pathfinder.jpg'),
            Text(
              'Pathfinder',
              style: Theme.of(context).textTheme.headline4,
            ),
            const Divider(
              color: Colors.white54,
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => EditProfilePage()));
              },
              leading: Icon(Icons.account_circle_rounded),
              title: Text('Profile'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => TravalGuide()));
              },
              leading: Icon(Icons.directions),
              title: Text('Travel Guide'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => NearbyPlaces()));
              },
              leading: Icon(Icons.search),
              title: Text('Search Places'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Tourist()));
              },
              leading: Icon(Icons.near_me),
              title: Text('Tourist Attraction'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) =>RaiasedButton()));
              },
              leading: Icon(Icons.contactless_outlined),
              title: Text('Contact Tracing'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => COVID()));},
              leading: Icon(Icons.ac_unit),
              title: Text('Covid Traker'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => HeatMapsButton()));
              },
              leading: Icon(Icons.maps_ugc),
              title: Text('Heat/Fire Maps'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Explore()));
              },
              leading: Icon(Icons.place),
              title: Text('Explore Places'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => ExplorPlace()));
              },
              leading: Icon(Icons.place),
              title: Text('Explore Places'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Tourist()));
              },
              leading: Icon(Icons.place_sharp),
              title: Text('Nearby Places'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => University()));
              },
              leading: Icon(Icons.cast_for_education),
              title: Text('Nearby University'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Mosque()));
              },
              leading: Icon(Icons.place),
              title: Text('Nearby Mosque'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Resturants()));
              },
              leading: Icon(Icons.add_location_alt_outlined),
              title: Text('Nearby Resturants'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Hotel()));
              },
              leading: Icon(Icons.hotel),
              title: Text('Nearby Hotels'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Police()));
              },
              leading: Icon(Icons.local_police),
              title: Text('Nearby Police Station'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Hospital()));
              },
              leading: Icon(Icons.local_hospital),
              title: Text('Nearby Hospital'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Parking()));
              },
              leading: Icon(Icons.local_parking),
              title: Text('Nearby Parking Area'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => NearbyPlacesButton()));
              },
              leading: Icon(Icons.place),
              title: Text('Nearby Other Places'),
            ),
            ListTile(
              onTap: () {Navigator.push(
                    context, MaterialPageRoute(builder: (_) => TravelBlog()));},
              leading: Icon(Icons.settings),
              title: Text('Travel Blog'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Currency()));
              },
              leading: Icon(Icons.money),
              title: Text('Currency Converter'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Notes()));
              },
              leading: Icon(Icons.notes),
              title: Text('Notes'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => PushNotification()));
              },
              leading: Icon(Icons.notifications),
              title: Text('Push Notification'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => BusSlider()));
              },
              leading: Icon(Icons.bus_alert),
              title: Text('Buy Bus Ticket'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => PlainSlider()));
              },
              leading: Icon(Icons.airplane_ticket),
              title: Text('Buy Plane Ticket'),
            ),

            ListTile(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Login()));
              },
              leading: Icon(Icons.logout),
              title: Text('Sign Out'),
            ),
            const Divider(
              color: Colors.white54,
            ),
            Container(
              height: 48,
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.share),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Text(
                      'Tell a Friend',
                      style: TextStyle(fontSize: 16),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 48,
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.help_outline),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Text(
                      'Help and Feedback',
                      style: TextStyle(fontSize: 16),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class DrawerItem {
  final String name;
  final IconData icon;

  const DrawerItem(this.name, this.icon);
}
