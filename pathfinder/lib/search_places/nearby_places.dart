
import 'package:flutter/material.dart';
import 'package:place_picker/place_picker.dart';

class NearbyPlaces extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => NearbyPlacesState();
}




class NearbyPlacesState extends State<NearbyPlaces> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Nearby Places')),
      body: Center(
        child: FlatButton(
          child: Text("Nearby Places Location"),
          onPressed: () {
            showPlacePicker();
          },
        ),
      ),
    );
  }

  void showPlacePicker() async {
    LocationResult result = await Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => PlacePicker("AIzaSyAWCOVaIbm9Qy0RqEUKe1SGdYfE1QQr2GE")));

    // Handle the result in your way
    print(result);
  }
}