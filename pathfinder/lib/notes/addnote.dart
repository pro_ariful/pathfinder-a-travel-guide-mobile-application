import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.white,
          accentColor: Colors.orange),
      home: Notes(),
    ));

class Notes extends StatefulWidget {
  @override
  _NotesState createState() => _NotesState();
}

class _NotesState extends State<Notes> {
  List whattodo = List();
  String input = "";
  @override
  void initState() {
    super.initState();
    whattodo.add('Johor');
    whattodo.add('UTM');
    whattodo.add('Skudai');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('List of Notes')),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Add Notes'),
                  content: TextField(onChanged: (String value) {
                    input = value;
                  }),
                  actions: <Widget>[
                    FlatButton(
                        onPressed: () {
                          setState(() {
                            whattodo.add(input);
                          });
                        },
                        child: Text('Add'))
                  ],
                );
              });
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      body: ListView.builder(
          itemCount: whattodo.length,
          itemBuilder: (BuildContext context, int index) {
            return Dismissible(
                key: Key(whattodo[index]),
                child: Card(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  child: ListTile(
                    title: Text(whattodo[index]),
                    trailing: IconButton(
                        icon: Icon(Icons.delete_outline),
                        onPressed: () {
                          setState(() {
                            whattodo.removeAt(index);
                          });
                        }),
                  ),
                ));
          }),
    );
  }
}