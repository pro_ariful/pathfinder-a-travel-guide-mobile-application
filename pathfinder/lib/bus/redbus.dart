import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class Redbus extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      
      title: "Pathfinder for Bus Tickets",
      home: Scaffold(
        
        appBar:AppBar(
          
          
          title: Text('Buy Bus Ticket Online'),
          backgroundColor: Color(0xFFFF1744),
          ),

          body: WebView(

            initialUrl: "https://www.redbus.my/",
            javascriptMode: JavascriptMode.unrestricted,
          ),
          
          ),
      
    );
  }
}