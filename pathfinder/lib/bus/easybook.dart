import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class EasyBook extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      
      title: "Pathfinder for Bus Tickets",
      home: Scaffold(
        
        appBar:AppBar(
          
          
          title: Text('Buy Bus Ticket Online'),
          backgroundColor: Color(0xFFFF1744),
          ),

          body: WebView(

            initialUrl: "https://www.easybook.com/en-my",
            javascriptMode: JavascriptMode.unrestricted,
          ),
          
          ),
      
    );
  }
}