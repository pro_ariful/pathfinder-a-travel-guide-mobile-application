import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class Klook extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      
      title: "Pathfinder for Bus Tickets",
      home: Scaffold(
        
        appBar:AppBar(
          
          
          title: Text('Buy Bus Ticket Online'),
          backgroundColor: Color(0xFFFF1744),
          ),

          body: WebView(

            initialUrl: "https://www.klook.com/en-MY/buses-31/1019-malaysia/",
            javascriptMode: JavascriptMode.unrestricted,
          ),
          
          ),
      
    );
  }
}