import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:pathfinder/bus/buslist.dart';
import 'redbus.dart';

class BusSlider extends StatefulWidget {

   BusSlider({Key key}) : super(key: key);
  

  @override
  _BusSliderState createState() => _BusSliderState();
}

class _BusSliderState extends State<BusSlider> {
  List<Slide> slides = [];
  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "redbus",
        description: "Book the best bus online tickets in Malaysia at super affordable prices",
        pathImage: "assets/images/redbus.png",
        backgroundColor: Color(0xfff5a623),
      ),
    );
    slides.add(
      new Slide(
        title: "easy book",
        description: "Best Mobile App to Book Bus Tickets, Train Tickets, Ferry Tickets, Affordable Car Rental and Tours in South East Asia",
        pathImage: "assets/images/easybook.jpg",
        backgroundColor: Color(0xff203152),
      ),
    );
    slides.add(
      new Slide(
        title: "catch that bus",
        description:
            "Buy Bus Tickets Online. Malaysia & Singapore. Search Tickets ... Skip the long queues and book your bus ticket from your phone anytime, anywhere",
        pathImage: "assets/images/catchthat.png",
        backgroundColor: Color(0xff9932CC),
      ),
    );
  }

  void onDonePress() {

    Navigator.push(
                    context, MaterialPageRoute(builder: (_) => BusListButton()));
    // Do what you want
    print("End of slides");
  }
  Widget build(BuildContext context) {
    return IntroSlider(
      slides: this.slides,
      onDonePress: this.onDonePress,
      
    );
  }
}