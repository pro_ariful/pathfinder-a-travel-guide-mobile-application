
import 'package:flutter/material.dart';
import 'package:pathfinder/bus/bus_online_ticket.dart';
import 'package:pathfinder/bus/catch_that_bus.dart';
import 'package:pathfinder/bus/easybook.dart';
import 'package:pathfinder/bus/klook.dart';
import 'package:pathfinder/bus/redbus.dart';




class BusListButton extends StatefulWidget {  
  @override  
  _BusListButtonState createState() => _BusListButtonState();  
}  
  
class _BusListButtonState extends State<BusListButton> {  
  @override  
  Widget build(BuildContext context) {  
    return MaterialApp(  
      home: Scaffold(  
          appBar: AppBar(  
            title: Text('Buy Plain Tickets'),  
            backgroundColor: Color(0xFFFF1744),
          ),  
          body: Center(child: ListView(children: <Widget>[  
             
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Red Bus", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Redbus()));
                },  
              ),  
            ), 

            ListTile(  
               
              title: OutlineButton(  
                child: Text("Easy Book", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => EasyBook()));
                },  
              ),  
            ),

            ListTile(  
               
              title: OutlineButton(  
                child: Text("Catch That Bus", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => CatchThatBus()));
                },  
              ),  
            ),
            ListTile(  
               
              title: OutlineButton(  
                child: Text("Klook", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => Klook()));
                },  
              ),  
            ),

            ListTile(  
               
              title: OutlineButton(  
                child: Text("Bus Online Ticket", style: TextStyle(fontSize: 20.0),),  
                highlightedBorderColor: Colors.red,  
                shape: RoundedRectangleBorder(  
                    borderRadius: BorderRadius.circular(15)),  
                onPressed: () {
                  Navigator.push(
                    context, MaterialPageRoute(builder: (_) => BusOnlineTicket()));
                },  
              ),  
            ),
            
           
             
          ]  
          ))  
      ),  
    );  
  }  
}  